package org.test.search.api.dto;

import lombok.Data;

@Data
public class CityWeatherDto {
    private String name;
    private String capital;
    private String numericCode;
    private String feelsLike;
    private String humidity;
    private int isDay;
    private String temp;
}
