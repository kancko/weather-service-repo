package org.test.search.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.test.search.api.dto.CityWeatherDto;

public interface SearchController {

    @GetMapping("/weather/{cur}")
    CityWeatherDto weather(@PathVariable String cur);

}
