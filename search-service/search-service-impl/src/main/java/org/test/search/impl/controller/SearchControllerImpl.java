package org.test.search.impl.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RestController;
import org.test.country.api.dto.CountryDto;
import org.test.country.client.CountryClient;
import org.test.search.api.controller.SearchController;
import org.test.search.api.dto.CityWeatherDto;
import org.test.weather.api.dto.WeatherDto;
import org.test.weather.client.WeatherClient;

@RestController
@RequiredArgsConstructor
public class SearchControllerImpl implements SearchController {
    private final WeatherClient weatherClient;
    private final CountryClient countryClient;

    @Override
    public CityWeatherDto weather(String cur) {
        CountryDto countryDto = countryClient.getCountry(cur).get(0);
        WeatherDto weatherDto = weatherClient.getWeather(countryDto.getCapital());
        CityWeatherDto dto = new CityWeatherDto();
        BeanUtils.copyProperties(countryDto, dto);
        BeanUtils.copyProperties(weatherDto.getCurrent(), dto);
        return dto;
    }
}
