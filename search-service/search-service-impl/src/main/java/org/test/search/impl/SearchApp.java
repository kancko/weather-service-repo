package org.test.search.impl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.test.country.client.CountryClient;
import org.test.weather.client.WeatherClient;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(clients = {WeatherClient.class, CountryClient.class})
public class SearchApp {

    public static void main(String[] args) {
        SpringApplication.run(SearchApp.class, args);
    }

}

