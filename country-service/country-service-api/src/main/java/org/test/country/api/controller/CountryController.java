package org.test.country.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.test.country.api.dto.CountryDto;

import java.util.List;

public interface CountryController {

    @GetMapping("/country/{cur}")
    List<CountryDto> getCountry(@PathVariable String cur);

}
