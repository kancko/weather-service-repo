package org.test.country.api.dto;

import lombok.Data;

@Data
public class CountryDto {
    private String name;
    private String capital;
    private String numericCode;
}
