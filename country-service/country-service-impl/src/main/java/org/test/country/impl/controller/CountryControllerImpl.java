package org.test.country.impl.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import org.test.country.api.controller.CountryController;
import org.test.country.api.dto.CountryDto;
import org.test.country.impl.client.CountryFeignClient;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CountryControllerImpl implements CountryController {

    private final CountryFeignClient client;

    @Override
    public List<CountryDto> getCountry(String cur) {
        return client.getCountry(cur);
    }

}
