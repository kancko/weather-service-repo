package org.test.country.impl.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.test.country.api.dto.CountryDto;

import java.util.List;

@FeignClient(name = "countries", url = "${feign.countries.url}")
public interface CountryFeignClient {

    @GetMapping("/currency/{cur}")
    List<CountryDto> getCountry(@PathVariable String cur);

}
