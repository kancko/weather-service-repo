package org.test.country.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.test.country.api.dto.CountryDto;

import java.util.List;

@FeignClient(name = "country-service")
public interface CountryClient {

    @GetMapping("/country/{cur}")
    List<CountryDto> getCountry(@PathVariable String cur);

}
