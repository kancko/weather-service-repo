package org.test.weather.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WeatherDto {

    private Current current;

    @Data
    public static class Current {
        @JsonProperty("feelslike_c")
        private String feelsLike;
        private String humidity;
        @JsonProperty("is_day")
        private int isDay;
        @JsonProperty("temp_c")
        private String temp;
    }
}
