package org.test.weather.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.test.weather.api.dto.WeatherDto;

public interface WeatherController {

    @GetMapping("/weather/{city}")
    WeatherDto getWeatherForCity(@PathVariable String city);

}
