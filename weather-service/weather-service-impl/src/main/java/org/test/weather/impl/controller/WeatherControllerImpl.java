package org.test.weather.impl.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import org.test.weather.api.controller.WeatherController;
import org.test.weather.api.dto.WeatherDto;
import org.test.weather.impl.client.WeatherFeignClient;

@RestController
@RequiredArgsConstructor
public class WeatherControllerImpl implements WeatherController {

    private final WeatherFeignClient client;

    @Override
    public WeatherDto getWeatherForCity(String city) {
        return client.getWeather(city);
    }

}
