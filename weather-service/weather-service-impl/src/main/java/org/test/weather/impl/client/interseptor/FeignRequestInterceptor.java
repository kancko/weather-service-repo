package org.test.weather.impl.client.interseptor;

import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.test.weather.impl.config.FeignConfiguration;

public class FeignRequestInterceptor {

    @Bean
    public RequestInterceptor apiKeyRequestInterceptor(FeignConfiguration configuration) {
        return requestTemplate -> requestTemplate.query(configuration.getName(), configuration.getValue());
    }

}
