package org.test.weather.impl.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.test.weather.api.dto.WeatherDto;
import org.test.weather.impl.client.interseptor.FeignRequestInterceptor;

@FeignClient(
        name = "weather",
        url = "${feign.weather.url}",
        configuration = FeignRequestInterceptor.class)
public interface WeatherFeignClient {

    @GetMapping("/current.json")
    WeatherDto getWeather(@RequestParam("q") String city);

}
