package org.test.weather.impl.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("weather.api-key")
public class FeignConfiguration {
     private String name;
     private String value;
}
