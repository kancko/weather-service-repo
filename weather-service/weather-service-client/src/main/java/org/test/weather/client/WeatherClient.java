package org.test.weather.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.test.weather.api.dto.WeatherDto;

@FeignClient(name = "weather-service")
public interface WeatherClient {

    @GetMapping("/weather/{city}")
    WeatherDto getWeather(@RequestParam String city);

}
